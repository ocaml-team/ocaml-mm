Source: ocaml-mm
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Kyle Robbertze <paddatrapper@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-buildinfo,
               dh-ocaml (>= 1.2),
               ocaml-dune (>= 2.8.0),
               libalsa-ocaml-dev,
               libao-ocaml-dev,
               libdune-ocaml-dev,
               libmad-ocaml-dev,
               libpulse-ocaml-dev,
               libtheora-ocaml-dev,
               ocaml
Standards-Version: 4.7.0
Homepage: https://github.com/savonet/ocaml-mm
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-mm.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-mm
Rules-Requires-Root: no

Package: libmm-ocaml
Architecture: any
Depends: ${misc:Depends}, ${ocaml:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Description: OCaml multimedia library -- runtime files
 OCaml-mm is a toolkit for audio and video processing
 in OCaml. It provides a standard interface and various
 usual manipulations on audio data, images and video data.
 .
 This package contains only the shared runtime stub libraries.

Package: libmm-ocaml-dev
Architecture: any
Depends: libmm-ocaml (= ${binary:Version}),
         ocaml-findlib,
         ${shlibs:Depends},
         ${misc:Depends},
         ${ocaml:Depends}
Provides: ${ocaml:Provides}
Description: OCaml multimedia library -- development files
 OCaml-mm is a toolkit for audio and video processing
 in OCaml. It provides a standard interface and various
 usual manipulations on audio data, images and video data.
 .
 This package contains all the development stuff you need to develop
 OCaml programs which use ocaml-mm.
